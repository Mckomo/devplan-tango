require_relative '../../support/helper.rb'

class TestTutorOperator < Minitest::Test
      
  context "a tutor operator" do
    
    should "extract tutor from activity" do
            
      tutor_xml = Nokogiri.XML( 
        "<zajecia>
          <nauczyciel moodle=\"-1043\">mgr Katarzyna Wójcik</nauczyciel>
        </zajecia>"
      ).xpath( '//nauczyciel' )
      teacher = TangoApp::Operator::Tutor.extract( tutor_xml).first
      
      assert_equal "mgr Katarzyna Wójcik", teacher.name
      assert_equal -1043, teacher.moodle_id
      
    end
    
    should "extract many teachers from activity" do
      
      tutors_xml = Nokogiri.XML( 
        "<zajecia>
          <nauczyciel moodle=\"-892\">dr inż. Janusz Morajda</nauczyciel>
          <nauczyciel moodle=\"-1043\">mgr Katarzyna Wójcik</nauczyciel>
        </zajecia>"
      ).xpath( '//nauczyciel' )
      
                        
      teachers = TangoApp::Operator::Tutor.extract( tutors_xml )
      
      assert_equal "dr inż. Janusz Morajda", teachers[0].name
      assert_equal -892, teachers[0].moodle_id 
      
      assert_equal "mgr Katarzyna Wójcik", teachers[1].name
      assert_equal -1043, teachers[1].moodle_id
     
    end
    
    should "extract teacher from his timetable" do
      
      timetable_xml = Nokogiri.XML( '<plan-zajec typ="N" id="8982" idcel="-396" nazwa="Ciuman Krystyna, prof. UEK dr hab." od="2014-03-22" do="2014-04-05">
      </plan-zajec>').at_xpath( 'plan-zajec' )
      
      teacher = TangoApp::Operator::Tutor.extract( timetable_xml )
      
      assert_equal "prof. UEK dr hab. Krystyna Ciuman", teacher.name
      assert_equal -396, teacher.moodle_id
     
    end
    
    should "not extract tutor with 'lecturer' in name (it's fake name)" do
     
      tutor_xml = Nokogiri.XML( 
        "<zajecia>
          <nauczyciel>mgr Lektorat Lektoraa</nauczyciel>
        </zajecia>"
      ).xpath( '//nauczyciel' )
      
      assert_equal 0, TangoApp::Operator::Tutor.extract( tutor_xml ).size
      
    end
    
    
  end
  
end