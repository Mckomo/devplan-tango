require_relative '../../support/helper.rb'

class TestPlaceOperator < Minitest::Test
      
  context "a place operator" do
    
    should "extract simple place from activity to one element array" do
      
      place_xml = Nokogiri.XML( "<sala>Bibl. 442</sala>" ).at_xpath( 'sala' )
      places = TangoApp::Operator::Place.extract( place_xml )
      
      assert  places.is_a? Array
      assert_equal 1, places.size 
      assert_equal "Bibl. 442", places[0].original_name
      
    end
    
    should "return array of places if there many places in one XML element" do
      
      multi_places_xml = Nokogiri.XML( '<sala>Paw.A 121 lab. Vista, Office07, Paw.C nowa aula</sala>' ).at_xpath( 'sala' )             
      places = TangoApp::Operator::Place.extract( multi_places_xml )       

      assert_equal 2, places.size
      assert_equal "Paw.A 121 lab. Vista", places[0].original_name
      assert_equal "Paw.C nowa aula", places[1].original_name
     
    end
    
  
  end
  
end