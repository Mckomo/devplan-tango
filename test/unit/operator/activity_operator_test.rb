require_relative '../../support/helper.rb'

class TestActivityOperator < Minitest::Test
      
  context "a activity operator" do

    should "extract all data from activity" do
            
      activity_xml = Nokogiri.XML( 
        "<zajecia>
          <termin>2014-02-25</termin>
          <dzien>Wt</dzien>
          <od-godz>11:20</od-godz>
          <do-godz>13:00</do-godz>
          <przedmiot>Analiza i projektowanie aplikacji</przedmiot>
          <typ/>
        </zajecia>"
      ).at_xpath( '//zajecia' )

      activity = TangoApp::Operator::Activity.extract( activity_xml )
      
      assert_equal '2014-02-25', activity.date
      assert_equal 'Wt', activity.day_of_week
      assert_equal '11:20', activity.starts_at
      assert_equal '13:00', activity.ends_at
      assert_equal 'Analiza i projektowanie aplikacji', activity.name
      assert_equal '', activity.category

    end

    should "capitalize category of activity" do

      activity_xml = Nokogiri.XML( 
        "<zajecia>
          <termin/>
          <dzien/>
          <od-godz/>
          <do-godz/>
          <przedmiot/>
          <typ>wykład do wyboru</typ>
        </zajecia>"
      ).at_xpath( '//zajecia' )

      activity = TangoApp::Operator::Activity.transform(TangoApp::Operator::Activity.extract( activity_xml ))

      assert_equal 'Wykład do wyboru', activity.category

      activity_xml = Nokogiri.XML( 
        "<zajecia>
          <termin/>
          <dzien/>
          <od-godz/>
          <do-godz/>
          <przedmiot/>
          <typ>ćwiczenia</typ>
        </zajecia>"
      ).at_xpath( '//zajecia' )

      activity = TangoApp::Operator::Activity.transform(TangoApp::Operator::Activity.extract( activity_xml ))

      assert_equal 'Ćwiczenia', activity.category

    end

    should "transform normal activity with active status" do

      activity_xml = Nokogiri.XML( 
        "<zajecia>
          <termin>2014-02-25</termin>
          <dzien>Wt</dzien>
          <od-godz>11:20</od-godz>
          <do-godz>13:00</do-godz>
          <przedmiot>Analiza i projektowanie aplikacji</przedmiot>
          <typ/>
        </zajecia>"
      ).at_xpath( '//zajecia' )

      activity = TangoApp::Operator::Activity.transform( TangoApp::Operator::Activity.extract( activity_xml ) )

      assert_equal 1, activity.state

    end

    should "transform postponed activity with postponed status" do

      activity_xml = Nokogiri.XML( 
        "<zajecia>
          <termin>2014-02-25</termin>
          <dzien>Wt</dzien>
          <od-godz>11:20</od-godz>
          <do-godz>13:00</do-godz>
          <przedmiot>Analiza i projektowanie aplikacji</przedmiot>
          <typ>Przeniesienie zajęć/typ>
        </zajecia>"
      ).at_xpath( '//zajecia' )

      activity = TangoApp::Operator::Activity.transform( TangoApp::Operator::Activity.extract( activity_xml ) )

      assert_equal 3, activity.state

    end


    should "transform remarked activity with remark status" do

      activity_xml = Nokogiri.XML( 
        "<zajecia>
          <termin>2014-02-25</termin>
          <dzien>Wt</dzien>
          <od-godz>11:20</od-godz>
          <do-godz>13:00</do-godz>
          <przedmiot>Analiza i projektowanie aplikacji</przedmiot>
          <uwagi>Kolokwium</uwagi>
          <typ/>
        </zajecia>"
      ).at_xpath( '//zajecia' )

      activity = TangoApp::Operator::Activity.transform( TangoApp::Operator::Activity.extract( activity_xml ) )

      assert_equal 2, activity.state

    end
    
  end
  
end