require_relative '../../support/helper'

class TestGroupOperator < Minitest::Test
  
  context "a group operator" do
    
    should "extract one teacher from activity" do
      
      groups_xml = Nokogiri.XML( "<zajecia>
          <grupa>
            KrDUZA2011Co, KrDUZA2011Mm, KrDUZa2011Ra
          </grupa>
        </zajecia>" ).at_xpath( '//grupa' )
      
      groups = TangoApp::Operator::Group.extract( groups_xml )
      
      assert_equal "KrDUZA2011Co", groups[0].name
      assert_equal "KrDUZA2011Mm", groups[1].name
      assert_equal "KrDUZa2011Ra", groups[2].name
      
    end
    
    should "extract group from his timetable" do
      
      timetable_xml = Nokogiri.XML( '<plan-zajec nazwa="KrDUZA2011Co" ></plan-zajec>').at_xpath( 'plan-zajec' )
      group = TangoApp::Operator::Group.extract( timetable_xml )
      
      assert_equal "KrDUZA2011Co", group.name
     
    end
    
  
  end
  
end