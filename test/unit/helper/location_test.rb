require_relative '../../support/helper.rb'

class TestLocation < Minitest::Test
      
  context "a simple location" do

    setup do
      @location = TangoApp::Helper::Location.new( {
        original_name: "Paw.C 512",
        building_type:  "pawilon",
        room_type: "sala",
        building_name_tokens: ["C"],
        room_name_tokens: ["512"]
      } )
    end

    should "have  string form of original name" do
      assert_equal "Paw.C 512", @location.to_s( :original )
    end

    should "have correct short string form" do
      assert_equal "Paw. C s. 512", @location.to_s( :short )
    end
    
    should "have correct string form" do
      assert_equal "Pawilon C sala 512", @location.to_s
    end

  end

  context "a location with complex room name" do

    setup do
      @location = TangoApp::Helper::Location.new( {
        original_name: "Bud.gł. Stara Aula",
        building_type:  "budynek",
        room_type: "aula",
        building_name_tokens: ["główny"],
        room_name_tokens: ["stara"]
      } )
    end

    should "have  string form of original name" do
      assert_equal "Bud.gł. Stara Aula", @location.to_s( :original )
    end

    should "have correct short string form" do
      assert_equal "Bud. gł. aula stara", @location.to_s( :short )
    end
    
    should "have correct string form" do
      assert_equal "Budynek główny aula stara", @location.to_s
    end

  end

  context "a complex location" do

    setup do
      @location = TangoApp::Helper::Location.new( {
        original_name: "30 koło kortów",
        building_type:  "budynek",
        building_name_tokens: ["koło", "kortów"],
        room_name_tokens: ["30"]
      } )
    end

    should "have  string form of original name" do
      assert_equal "30 koło kortów", @location.to_s( :original )
    end

    should "have correct short string form" do
      assert_equal "Bud. koło kortów s. 30", @location.to_s( :short )
    end

    should "have correct string form" do
      assert_equal "Budynek koło kortów sala 30", @location.to_s
    end

    
  end

  context "a outer city location" do

    setup do
      @location = TangoApp::Helper::Location.new( {
        original_name: "ZOD Nowy Targ lab. 32",        
        city_name_tokens: ["nowy", "targ"],
        room_type: "laboratorium",
        room_name_tokens: ["32"]
      } )
    end

    should "have  string form of original name" do
      assert_equal "ZOD Nowy Targ lab. 32", @location.to_s( :original )
    end

    should "have short string form" do
      assert_equal "Nowy Targ lab. 32", @location.to_s( :short )
    end

    should "have string form" do
      assert_equal "Nowy Targ laboratorium 32", @location.to_s
    end

  end

  context "a location string form" do

    should "always start with capital letter" do

      location = TangoApp::Helper::Location.new( {
        original_name: "Rakowicka 16 sala 11",        
        building_name_tokens: ["rakowicka", "16"],
        room_type: "sala",
        room_name_tokens: ["11"]
      } )

      assert_equal 'R', location.to_s( :short )[0, 1]
      assert_equal 'R', location.to_s[0, 1]

    end

  end

end