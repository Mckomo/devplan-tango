require_relative '../../support/helper.rb'

class TestLocationParser < Minitest::Test
      
  context "a location parser" do
    
    setup do
      @parser = TangoApp::Helper::LocationParser.new
    end

    should "return array of Locations" do

      locations = @parser.parse( "Paw.B 057, Paw.B 058, Paw.B 059" )

      assert_equal Array, locations.class
      assert_equal 3, locations.size

      locations.each do |l|
        assert_equal TangoApp::Helper::Location, l.class 
      end

    end

    should "save original name of location" do
      
    end

    should "skip irrelevant location candidates" do

      locations = @parser.parse( "Paw.A lab. 116 , Office03" )
      assert_equal 1, locations.size

    end

    should "set special values for special cases" do

      location = @parser.parse( "Ust.s.401" ).first
      
      assert_equal "pawilon", location.building_type
      assert_equal ["u"], location.building_name_tokens

      location = @parser.parse( "30 koło kartow" ).first
      
      assert_equal "budynek", location.building_type

    end

    should "parse simple location" do
      
      location = @parser.parse( "Paw.C sala A" ).first
      
      assert_equal "pawilon", location.building_type
      assert_equal "sala", location.room_type
      assert_equal ["c"], location.building_name_tokens
      assert_equal ["a"], location.room_name_tokens
      
    end

    should "parse simple locations" do
      
      locations = @parser.parse( "Paw.C sala A, Budynek G lab. 12" )
      
      assert_equal 2, locations.size

      assert_equal "pawilon", locations[0].building_type
      assert_equal "sala", locations[0].room_type
      assert_equal ["c"], locations[0].building_name_tokens
      assert_equal ["a"], locations[0].room_name_tokens

      assert_equal "budynek", locations[1].building_type
      assert_equal "laboratorium", locations[1].room_type
      assert_equal ["g"], locations[1].building_name_tokens
      assert_equal ["12"], locations[1].room_name_tokens
      
    end

    should "parse location with missing elements" do

      location = @parser.parse( "Paw.B 152" ).first
      
      assert_equal "pawilon", location.building_type
      assert_equal ["b"], location.building_name_tokens
      assert_equal ["152"], location.room_name_tokens

    end

    should "parse location with unordered elements" do

      location = @parser.parse( "Paw.A 116 lab." ).first
      
      assert_equal "pawilon", location.building_type
      assert_equal "laboratorium", location.room_type
      assert_equal ["a"], location.building_name_tokens
      assert_equal ["116"], location.room_name_tokens


      location = @parser.parse( "208 paw.U" ).first
      
      assert_equal "pawilon", location.building_type
      assert_equal "sala", location.room_type
      assert_equal ["u"], location.building_name_tokens
      assert_equal ["208"], location.room_name_tokens


      location = @parser.parse( "Prac. mikrob.2 paw. D" ).first
      
      assert_equal "pawilon", location.building_type
      assert_equal "pracownia", location.room_type
      assert_equal ["d"], location.building_name_tokens
      assert_equal ["mikrobiologiczna", "2"], location.room_name_tokens

    end

    should "filter out irrelevant elements from location" do

      location = @parser.parse( "Paw.A 014 lab. Win8, Office03" ).first
      
      assert_equal "pawilon", location.building_type
      assert_equal "laboratorium", location.room_type
      assert_equal ["a"], location.building_name_tokens
      assert_equal ["014"], location.room_name_tokens

      location = @parser.parse( "Paw.A 122 lab. Vista, Office07" ).first
      
      assert_equal "pawilon", location.building_type
      assert_equal "laboratorium", location.room_type
      assert_equal ["a"], location.building_name_tokens
      assert_equal ["122"], location.room_name_tokens

      location = @parser.parse( "SJO 301 paw. F" ).first
      
      assert_equal "pawilon", location.building_type
      assert_equal "sala", location.room_type
      assert_equal ["f"], location.building_name_tokens
      assert_equal ["301"], location.room_name_tokens

    end

    should "parse location with complex room name" do

      location = @parser.parse( "Ust.s.401 - lab. przechow" ).first
      
      assert_equal "pawilon", location.building_type
      assert_equal "laboratorium", location.room_type
      assert_equal ["u"], location.building_name_tokens
      assert_equal ["401"], location.room_name_tokens

      location = @parser.parse( "Bud.gł. Stara Aula" ).first
      
      assert_equal "budynek", location.building_type
      assert_equal "aula", location.room_type
      assert_equal ["główny"], location.building_name_tokens
      assert_equal ["stara"], location.room_name_tokens

    end


    should "parse location with complex building name" do

      location = @parser.parse( "30 koło kortów" ).first
      
      assert_equal "budynek", location.building_type
      assert_equal "sala", location.room_type
      assert_equal ["koło", "kortów"], location.building_name_tokens
      assert_equal ["30"], location.room_name_tokens

      location = @parser.parse( "Rakowicka 16 sala 13" ).first
      
      assert_equal "sala", location.room_type
      assert_equal ["rakowicka", "16"], location.building_name_tokens
      assert_equal ["13"], location.room_name_tokens

    end

    should "parse location with outer cities" do

      location = @parser.parse( "ZOD Nowy Targ sala nr 32 lab." ).first
      
      assert_equal ["nowy", "targ"], location.city_name_tokens
      assert_equal "", location.building_type
      assert_equal "laboratorium", location.room_type
      assert_equal [], location.building_name_tokens
      assert_equal ["32"], location.room_name_tokens

      location = @parser.parse( "ZOD Dębica lab.komp. 3 paw.A" ).first
      
      assert_equal ["dębica"], location.city_name_tokens
      assert_equal "pawilon", location.building_type
      assert_equal "laboratorium", location.room_type
      assert_equal ["a"], location.building_name_tokens
      assert_equal ["komputerowe", "3"], location.room_name_tokens

    end
  
  end
  
end