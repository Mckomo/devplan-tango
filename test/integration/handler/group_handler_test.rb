require_relative '../../support/helper.rb'

class GroupHandlerTest < Minitest::Test

    include TangoApp

    # For reference look at test/support/xml/group.xml file
    context "a trigger of group handler" do
        
        # Trigger once group handler
        setup do
            @@group_triggered ||= begin
                truncate_database
                cache = get_cache
                document = Nokogiri::XML( File.open( 'test/support/xml/group.xml' ) )
                handler = TangoApp::Handler::Group.new( '/index', document, cache )
                handler.trigger
                cache.buffer.release_all()
            end
        end

        should "save new group" do

            new_group = Model::Group.first

            assert_equal 1, Model::Group.count
            assert_equal 1, new_group.id
            assert_equal "KrDZIs3011Io", new_group.name

        end

        should "not save elective activities" do
            assert_equal 0, Model::Activity.where( name: "Marketing strategiczny" ).count
        end

        should "not save language course activities" do
            assert_equal 0, Model::Activity.where( name: "Marketing strategiczny" ).count
        end

        should "not save same activities" do
            assert_equal 1, Model::Activity.where( name: 'Sieci semantyczne', date: '2014-02-24' ).count
        end

        should "not save same places" do
            assert_equal 1, Model::Place.where( original_name: 'Paw.C sala A' ).count
        end

        should "save all non elective and language course activities from group's timetable" do

            assert_equal 3, Model::Activity.count
            assert_equal 0, Model::Activity.where( name: "Język obcy" ).count
            assert_equal 0, Model::Activity.where( name: "Marketing strategiczny" ).count

            assert_equal 'Teoria grafów', Model::Activity.find( 1 ).name
            assert_equal 'Statystyka opisowa', Model::Activity.find( 2 ).name
            assert_equal 'Sieci semantyczne', Model::Activity.find( 3 ).name

        end

        should "save the group as only participant of activities" do

            assert_equal 3, Model::Participation.count
            assert_equal [1], Model::Participation.pluck( :group_id ).uniq

        end

        should "save all tutors assigned to group's activities" do
            
            assert_equal 4, Model::Tutor.count

            assert_equal "prof. UEK Paweł Lula", Model::Tutor.find( 1 ).name
            assert_equal "dr Paweł Wołoszyn", Model::Tutor.find( 2 ).name
            assert_equal "dr Agnieszka Wałęga", Model::Tutor.find( 3 ).name
            assert_equal "dr Grażyna Paliwoda-Pękosz", Model::Tutor.find( 4 ).name

        end

        should "save all tutor assignment to activities" do
            
            # First activity has 2 tutors
            assignment = Model::Assignment.where( activity_id: 1 )
            assert_equal 2, assignment.count
            assert_equal [1, 2], assignment.pluck( :tutor_id )

            assignment = Model::Assignment.where( activity_id: 2 )
            assert_equal [3], assignment.pluck( :tutor_id )

            assignment = Model::Assignment.where( activity_id: 3 )
            assert_equal [4], assignment.pluck( :tutor_id )

        end

        should "save all places where group's activities are located" do
            
            assert_equal 3, Model::Place.count

            assert_equal "Paw.C sala A", Model::Place.find( 1 ).original_name
            assert_equal "Paw.E sala G", Model::Place.find( 2 ).original_name
            assert_equal "Paw.E sala H", Model::Place.find( 3 ).original_name

        end

        should "save all activities' localizations" do
            
            localization = Model::Localization.where( activity_id: 1 )
            assert_equal 1, localization.count
            assert_equal [1], localization.pluck( :place_id )

            localization = Model::Localization.where( activity_id: 2 )
            assert_equal 1, localization.count
            assert_equal [1], localization.pluck( :place_id )
    
            # Last activity has 2 places
            localization = Model::Localization.where( activity_id: 3 )
            assert_equal 2, localization.count
            assert_equal [2, 3], localization.pluck( :place_id )

        end

  end
  
end