require_relative '../../support/helper.rb'

class TutorHandlerTest < Minitest::Test

    include TangoApp

    # For reference look at test/support/xml/group.xml file
    context "a trigger of tutor handler" do
        
        # Trigger once group handler
        setup do
            @@tutor_triggered ||= begin
                truncate_database
                cache = get_cache
                document = Nokogiri::XML( File.open( 'test/support/xml/tutor.xml' ) )
                handler = TangoApp::Handler::Tutor.new( '/index', document, cache )
                handler.trigger
                cache.buffer.release_all()
            end
        end

        should "not save same groups" do
            assert_equal 1, Model::Group.where( name: 'KrDZIs1012' ).count
        end

        should "save new tutor" do

            tutor = Model::Tutor.first

            assert_equal 1, Model::Tutor.count
            assert_equal 1, tutor.id

        end

        should "save tutor's name in standardized form" do
            assert_equal 'dr Paweł Wołoszyn', Model::Tutor.first.name
        end


        should "save all groups which have activities with the tutor" do
            
            assert_equal 5, Model::Group.count

            assert_equal 'KrDUIe2011In', Model::Group.find( 1 ).name
            assert_equal 'KrDZIs2011', Model::Group.find( 2 ).name
            assert_equal 'KrDZIs2012', Model::Group.find( 3 ).name
            assert_equal 'KrDZIs1012', Model::Group.find( 4 ).name
            assert_equal 'KrDZIs1011', Model::Group.find( 5 ).name

        end
 
        should "save all groups' participation of the tutor's activities localizations" do
            
            # First activity has 3 groups
            participation = Model::Participation.where( activity_id: 1 )
            assert_equal 3, participation.count
            assert_equal [1, 2, 3], participation.pluck( :group_id )

            participation = Model::Participation.where( activity_id: 2 )
            assert_equal 1, participation.count
            assert_equal [4], participation.pluck( :group_id )
    
            # Last activity has 2 places
            participation = Model::Participation.where( activity_id: 3 )
            assert_equal 2, participation.count
            assert_equal [5, 4], participation.pluck( :group_id )

        end

  end
  
end