require 'rubygems'
require 'bundler/setup'

require 'minitest/autorun'
require 'shoulda/context'
require 'mocha/setup'

require 'nokogiri'
require 'active_record'
require 'activerecord-nulldb-adapter'
require 'tango'

require_relative '../../app/bootstrap.rb'

ActiveRecord::Base.configurations = YAML::load( File.open( File.expand_path( '../../../config/database.yml', __FILE__ ) ) )
ActiveRecord::Base.establish_connection :test

def truncate_database
	Tango::AbstractModel.subclasses.each do |m|
		Tango::AbstractModel.connection.execute("DELETE from #{m.table_name}; VACUME")
	end
end

def get_cache
	Tango::Resource::Cache.new.tap do |c|
		Tango::AbstractModel.subclasses.each do |m|
			 c.register( Tango::Kernel.symbolize(m) ) do |models|
			 	models.each { |m| m.save }
			 end
		end
	end
end