module TangoApp
  
  # Filters for Tango App runtime
  # 
  # @author Mckomo
  class App < ::Tango::App

    VERSION = "0.3.0"
    
    def before
    end
    
    def after
      
      # Change database in Cash API to currently used by Tango
      begin
        response = @http_client.post("#{@config['cash_url']}/api/settings", :body => { database: @db_locker.unlocked  }) 
        raise IOError unless response.code == 201
      rescue => e
        @logger.fatal("Cannot change Cash API database. Error: #{e}.")
      end
      
    end
    
  end
end
      