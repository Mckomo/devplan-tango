module TangoApp
  module Model
    
    # @author Mckomo
    class Group < ::Tango::AbstractModel
  
      # has_many :appointments 
      # has_many :activities, :through => :appointments
      
      def cache_key
        name
      end
      
      def self.persistent?
        true
      end
  
    end
    
  end
end