module TangoApp
  module Model
    
    # @author Mckomo
    class Activity < ::Tango::AbstractModel
      
      # has_many :participations
      # has_many :groups, :through => :participations
      # has_many :assignments
      # has_many :tutors, :through => :assignments
      # has_many :places, :through => :localizations
      # has_many :localizations

      attr_accessor :places
      
      @@states = {
        0 => "Nieaktywne", # inactive
        1 => "Aktywne", # active
        2 => "Posiada uwagi", # has remarks
        3 => "Przeniesione", # postponed
      }.freeze
      
      def self.persistent?
        false
      end
      
      def cache_key
        ( places.map { |p| p.id } + [ name, category, date, starts_at, ends_at ] ).join(';')
      end
      
      # Test whether activity is elective
      #
      # @params activity_params [Hash]
      # @return [Boolean]  
      def elective?
        name =~ /do wyboru/i || category =~ /do wyboru/i 
      end
      
      # Test whether particular activity is postponed
      #
      # @params activity_params [Hash]
      # @return [Boolean]  
      def postponed?
        category =~ /przenie[a-z]* zaję/iu
      end
      
      # Test whether activity is general language course ( but no concrete language specified )  
      #
      # @params activity_params [Hash]
      # @return [Boolean]    
      def language_course?
        name =~ /język obcy/iu || category =~ /lektorat/i
      end
      
      def blocked?
        name =~ /zablokowana/iu
      end
      
    end
    
  end 
end
