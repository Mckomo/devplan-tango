module TangoApp
  module Model
    
    # @author Mckomo
    class Participation < ::Tango::AbstractModel
  
      # belongs_to :group
      # belongs_to :activity
      
      def cache_key
        [ activity_id, group_id ]
      end
      
      def self.persistent?
        false
      end
  
    end
    
  end
end