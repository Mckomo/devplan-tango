module TangoApp
  module Model
    
    # @author Mckomo
    class Assignment < ::Tango::AbstractModel
      
      # belongs_to :tutor
      # belongs_to :activity
      
      def cache_key
        [ activity_id, tutor_id ]
      end
      
      def self.persistent?
        false
      end
      
    end
    
  end
end
