module TangoApp
  module Model
    
    # @author Mckomo
    class Place < ::Tango::AbstractModel
      
      # has_many :activities
      
      def cache_key
        original_name
      end
      
      def self.persistent?
        true
      end
      
    end
    
  end
end