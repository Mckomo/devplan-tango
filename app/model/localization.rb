module TangoApp
  module Model
    
    # @author Mckomo
    class Localization < ::Tango::AbstractModel
      
      # belongs_to :place
      # belongs_to :activity
      
      def cache_key
        [ activity_id, place_id ]
      end
      
      def self.persistent?
        false
      end
  
    end
    
  end
end