module TangoApp
  module Model
    
    # @author Mckomo
    class Tutor < ::Tango::AbstractModel
  
      # has_many :assignments 
      # has_many :activities, :through => :assignments
      
      def cache_key
        name
      end
      
      def self.persistent?
        true
      end
      
    end
    
  end
end
