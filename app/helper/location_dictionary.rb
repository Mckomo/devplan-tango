module TangoApp
	module Helper 
		
		# Standard dictionary used by the Location Parser to translate shortcuts
		#
		# @author Mckomo
		class LocationDictionary

			@@dictionary = {
				# room types
				"s."        => "sala",
				"pr."		=> "pracownia",
				"prac."   	=> "pracownia",
				"lab."		=> "laboratorium",
				# room names
				"sem."		=> "seminaryjna",
				"przemy."  	=> "przemysłowa",
				"fiz."      => "fizyczne",
				"spoż."     => "spożywcza",
				"chem."     => "chemiczna",
				"mikrob."   => "mikrobiologiczna",
				"przem."	=> "przemysłowa",
				"komp."     => "komputerowe",
				"an."		=> "analiza",
				"sens." 	=> "sensoryczna",
				# building types
				"paw."      => "pawilon",
				"bibl."     => "bibloteka",
				"bud."      => "budynek",
				# building names
				"sienk."    => "sienkiewicza",
				"gł."       => "główny",
				"ust."      => "ustronie"
			}.freeze

			@@inverted_dictionary = @@dictionary.invert.tap { |d|
				d.delete( "sienkiwiecza" )
			}.freeze
			
			def self.dictionary( invert = false )
				invert ? @@inverted_dictionary : @@dictionary
			end

		end

	end
end