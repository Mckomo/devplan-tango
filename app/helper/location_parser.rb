module TangoApp
  module Helper
  
    # C language style - state machine localization parser
    #
    # @note   Location Paraser is mainly used by TangoApp::Operator::Place to obtain uniform format 
    # @author Mckomo
    class LocationParser
      
      OUTHER_CITY_PREFIX = "zod"
      
      FIRST_TOKEN_MODE = 1
      CITY_NAME_MODE = 2
      BUILDING_NAME_MODE = 3
      ROOM_NAME_MODE = 4
      
      @@room_types = [
        "sala",
        "aula",
        "pracownia",
        "laboratorium",
      ]
      
      @@building_types = [
        "pawilon",
        "budynek",
        "bibloteka",
      ]
      
      # Constructor
      def initialize( dictionary = nil )
        @dictionary = dictionary || LocationDictionary.dictionary
      end

      # Parse location candidates
      #
      # @param  [String]
      # @return [Array]   List of parsed Locations 
      def parse( location_params_candidates )
        
        locations = []
        
        extract_names( location_params_candidates ).each do |location_name|

          @mode = FIRST_TOKEN_MODE

          location_params = {
            original_name: location_name,
            building_type:  "",
            room_type: "",
            city_name_tokens: [],
            building_name_tokens: [],
            room_name_tokens: []
          } 
          
          tokenize( location_name ).each do |t|

            token = translate( t )
            
            case @mode
            when FIRST_TOKEN_MODE
              parse_first( token, location_params )
            when CITY_NAME_MODE
              parse_city_name( token, location_params )
            when BUILDING_NAME_MODE
              parse_building_name( token, location_params )
            when ROOM_NAME_MODE
              parse_room_name( token, location_params )
            end

          end

          locations << TangoApp::Helper::Location.new( special_cases( location_params ) )
          
        end
        
        locations
        
      end
      
      private
      
      def parse_first( token, location_params )
        
        if token == OUTHER_CITY_PREFIX
          @mode = CITY_NAME_MODE; return
        end
        
        if building_type?( token )
          location_params[:building_type] = token
          @mode = BUILDING_NAME_MODE; return
        end

        if room_type?( token )
          location_params[:room_type] = token
          @mode = ROOM_NAME_MODE; return
        end

        if starts_numeric?( token )
          location_params[:room_name_tokens] << token
          @mode = BUILDING_NAME_MODE; return
        end

        location_params[:building_name_tokens] << token
        @mode = BUILDING_NAME_MODE
        
      end
      
      def parse_city_name( token, location_params )

        if building_type?( token )
          location_params[:building_type] = token
          @mode = BUILDING_NAME_MODE; return
        end

        if room_type?( token )
          location_params[:room_type] = token
          @mode = ROOM_NAME_MODE; return
        end

        location_params[:city_name_tokens] << token
        
      end

      def parse_building_name( token, location_params )

        if room_type?( token )
          location_params[:room_type] = token
          @mode = ROOM_NAME_MODE; return
        end

        if building_type?( token )
          location_params[:building_type] = token; return
        end

        location_params[:building_name_tokens] << token

        unless location_params[:building_type].blank?
           @mode = ROOM_NAME_MODE
        end

      end

      def parse_room_name( token, location_params )

        if building_type?( token )
          location_params[:building_type] = token
          @mode = BUILDING_NAME_MODE; return
        end

        if room_type?( token )
          location_params[:room_type] = token; return;
        end

        location_params[:room_name_tokens] << token

      end

      # Deal with special cases
      #
      # @param [Hash]
      # @return [Hash]
      def special_cases( location_params )

        location_params.tap do |p|

          if p[:building_name_tokens].first == "ustronie"
            p[:building_type] = "pawilon"
            p[:building_name_tokens] = ["u"]
          end

          if p[:building_type].blank? and p[:building_name_tokens].first == "koło"
            p[:building_type] = "budynek"
          end

        end

      end
      
      def translate( token )
        @dictionary[token] || token
      end
      
      # Extract correct location_params names
      #
      # @param  [String]
      # @return [Array]
      def extract_names( location_params_candidates )
        location_params_candidates.split( ',' )
          .delete_if { |n| 
            n =~ /office/ui  # Remove names that are irrelevant  
          }.map { |n| 
            n.strip
          }.reject { |n|
            n.blank?
          }
      end
      
      def tokenize( location_params_name )
        location_params_name.gsub( /\.(\S)/u, '. \1' ) # Add space between words separated only by dot
          .gsub( /(\S)\s\./, '\1.' ) # Remove space before dot 
          .gsub( /\(.*\)/u, '' ) # Remove phrases in brackets
          .split # Split to tokens
          .reject { |t|
             t =~ /-|win\d|xp|vista|office\d|krak|opakowa|przechow|sjo|nr/ui # Remove tokens that are irrelevant 
          }.reject { |t|
             t.blank? # Remove tokens blank tokens
          }.map { |n| 
            n.downcase 
          }  # Downcase tokens
      end
      
      def building_type?( token )
        @@building_types.include?( token )
      end
      
      def room_type?( token )
        @@room_types.include?( token )
      end
      
      def starts_numeric?( token )
        /^[[:digit:]]/.match( token )
      end

      def starts_capital?( token )
        /^[[:upper:]]/.match( token )
      end

    end
    
  end
end