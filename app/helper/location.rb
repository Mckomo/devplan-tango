module TangoApp
	module Helper 
		
		# Location object crated by Location Parser
		#
		# @author Mckomo
		class Location
		
			attr_reader :original_name
			attr_reader	:building_type
			attr_reader :room_type
			attr_reader :city_name_tokens
			attr_reader :building_name_tokens
			attr_reader :room_name_tokens

			# Constructor of Location
			#
			# @param 	params 		[Hash]
			# @param 	dictionary 	[Hash]	Dictionary used to created shortcuts
			# @return 				[TangoApp::Helper::Location]
			def initialize( params = {}, dictionary = nil )

				if params[:original_name].blank?
					raise ArgumentError, "Cannot create location without it's original name."
				end

				@original_name = params[:original_name]
				@building_type = params[:building_type] || ""
				@room_type = params[:room_type] || ""
				@city_name_tokens = params[:city_name_tokens] || []
				@building_name_tokens = params[:building_name_tokens] || []
				@room_name_tokens = params[:room_name_tokens] || []

				@dictionary = dictionary || LocationDictionary::dictionary( true )

		        if @room_type.blank?
		          @room_type = "sala"
		        end

			end

			# String version of objects in given scope
			#
			# @param 	scope	[Symbol]	Can by :original, :short, :full
			# @return 			[String] 
			def to_s( scope = :full )

				return @original_name if scope == :original

				short_version = scope == :short ? true : false

				[
					city( short_version ),
					building( short_version ),
					room( short_version )
				].reject { |p|
					p.blank?
				}.join( ' ' )

			end

			private

			def city( short_version )
				
				return nil unless outer_city?

				@city_name_tokens.map { |t| 
					t.capitalize 
				}.join(' ')

			end

			def building( short_version )

				formated_building_type = format_token( @building_type, short_version )
				formated_building_name = @building_name_tokens.map { |t| 
					format_token( t, short_version ) 
				}.join(' ')

				return formated_building_type if formated_building_name.blank?

				formated_building = "#{formated_building_type} #{formated_building_name}".strip
				formated_building[0, 1].capitalize + formated_building[1 .. -1] # Uppercase first letter 
				
			end

			def room( short_version )
				
				formated_room_type = format_token( @room_type, short_version )
				formated_room_name = @room_name_tokens.map { |t| 
					format_token( t, short_version ) 
				}.join(' ')

				return nil if formated_room_name.blank?
				
				"#{formated_room_type} #{formated_room_name}"

			end

			def outer_city?
				@city_name_tokens.size > 0 and @city_name_tokens.first != "kraków" 
			end


			def format_token( token, short_version )
				
				if token.length < 4
					return token.upcase
				end

				short_version ? shortcut( token ) : token

			end

			def shortcut( token )
				@dictionary[token] || token
			end

		end

	end
end