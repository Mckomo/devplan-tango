module TangoApp
  module Handler
    
      # Handler of website index
      #
      # @uri /index.php?xml
      # @author Mckomo
    	class Index < Tango::ETL::HandlerInterface
        
        def links
        
          # Map category letters to URL link format
          ['G', 'N'].map do |letter|
            "/index.php?typ=#{letter}&xml"
          end
        
        end
    
        def trigger; end
      
        # applicable 
        def self.applicable?( short_link )
          short_link =~ /^\/(index\.php){0,1}\?xml$/
        end 
      
      end
      
  end
end
