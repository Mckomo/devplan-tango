module TangoApp
  module Handler
    
      # Handler of website index
      #
      # @uri /index.php?typ=:category&xml
      # @author Mckomo
    	class Tutor < Tango::ETL::HandlerInterface
      
        def links
          nil
        end
    
        def trigger
        
          # Extract timetable's tutor from root element
          tutor = Operator::Tutor.extract( @document.at_xpath( '/plan-zajec' ) )
        
          # Load it to cache
          tutor = @cache.load( :tutor, tutor ) do
            Operator::Tutor.transform( tutor )
          end
        
          # Go through each activity to load them it db
          @document.xpath( '//zajecia' ).each do |activity_xml|
          
            # Extract activity
            activity =  Operator::Activity.extract( activity_xml )
            
            next if activity.blocked?
            
            places = activity.places = Operator::Place.extract( activity_xml.at_xpath( 'sala' ) ).map do |place|
              @cache.load( :place, place ) do |p|
                Operator::Place.transform( p )
              end
            end
          
            # Transform activity, but don't set it's id
            activity = @cache.load( :activity, activity ) do |a|
              Operator::Activity.transform( a )
            end
            
            tutor = @cache.load( :tutor, tutor ) do |t|
              Operator::Tutor.transform( t )
            end
            
            assignment = Model::Assignment.new( activity_id: activity.id, tutor_id: tutor.id )
            
            @cache.load( :assignment, assignment ) do
              Operator::Assignment.transform( assignment )
            end

            places.each do |p|

              localization = Model::Localization.new( activity_id: activity.id, place_id: p.id )
              
              @cache.load( :localization, localization ) do
                Operator::Localization.transform( localization )
              end

            end 
          
            # Load groups
            groups = Operator::Group.extract( activity_xml.at_xpath( 'grupa' ) )
                    
            # Iterate groups to load activity with each group
            groups.each do |group|
              
              # Create new group for elective activities
              if activity.elective?
                group.name = Operator::Group.elective_name( group, activity )
              end
            
              group = @cache.load( :group, group ) do |g|
                Operator::Group.transform( g ) 
              end
              
              participation = Model::Participation.new( activity_id: activity.id, group_id: group.id )
          
              @cache.load( :participation, participation ) do |p|
                Operator::Participation.transform( p )
              end
            
            end

          end
        end
      
        def self.applicable?( short_link )
          # return false
          short_link =~ /^\/(index\.php){0,1}\?typ=N&id=[0-9]{1,6}&okres=[12]&xml$/
        end 
      
      end
      
  end
end
