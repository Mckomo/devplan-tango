module TangoApp 
  module Handler
      
    # Handler of the group timetable
    #
    # @uri /index.php?typ={:category}&xml
    # @author Mckomo
  	class Group < Tango::ETL::HandlerInterface
      
      def links; end
    
      def trigger
        
        # Extract group from root element
        group = Operator::Group.extract( @document.at_xpath( '/plan-zajec' ) )
        
        # Load from/to cache
        group = @cache.load( :group, group ) do 
          Operator::Group.transform( group )
        end
        
        return unless group # At the stage, group has to exist
        
        # Go through each activity to load them it db
        @document.xpath( '//zajecia' ).each do |activity_xml|
          
          # Extract an activity params from XML
          activity = Operator::Activity.extract( activity_xml ).tap do |a|
            a.places = []
          end
          
          # Skip if activity is language course
          next if activity.language_course?
          
          # Skip if activity is elective ( other handlers take care of elective activities )
          next if activity.elective?
          
          places = activity.places = Operator::Place.extract( activity_xml.at_xpath( 'sala' ) ).map do |place|
            @cache.load( :place, place ) do |p|
              Operator::Place.transform( p )
            end
          end

          activity = @cache.load( :activity, activity ) do |a|
            Operator::Activity.transform( a )
          end
          
          participation = Model::Participation.new( activity_id: activity.id, group_id: group.id )
          
          @cache.load( :participation, participation ) do |p|
            Operator::Participation.transform( p )
          end
          
          tutors = Operator::Tutor.extract( activity_xml.xpath( 'nauczyciel' ) )
          
          tutors.each do |tutor|
            
            tutor = @cache.load( :tutor, tutor ) do |t|
              Operator::Tutor.transform( t )
            end
            
            assignment = Model::Assignment.new( activity_id: activity.id, tutor_id: tutor.id )
            
            @cache.load( :assignment, assignment ) do
              Operator::Assignment.transform( assignment )
            end
            
          end

          places.each do |p|

            localization = Model::Localization.new( activity_id: activity.id, place_id: p.id )
            
            @cache.load( :localization, localization ) do
              Operator::Localization.transform( localization )
            end

          end 
                      
        end
        
      end
      
      # applicable 
      def self.applicable?( short_link )
        short_link =~ /^\/(index\.php){0,1}\?typ=G&id=[0-9]{1,6}&okres=[12]&xml$/
      end 
      
    end
      
  end
end
