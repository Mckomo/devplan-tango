module TangoApp
  module Handler
    
      # Handler of website index
      #
      # @uri /index.php?typ=:category&xml
      # @author Mckomo
    	class Categories < Tango::ETL::HandlerInterface
      
        def links
          
          links = []
          
          # Get all group categories in timetable
          @document.xpath( '//zasob' ).each do |timetable_ref|
            # Get category letter of a timetable
            letter = timetable_ref.attr( 'typ' )
            # Get timetable id
            id = timetable_ref.attr( 'id' )
            # Append new link to a timetable with full duration
            links << "/index.php?typ=#{letter}&id=#{id}&okres=2&xml"
          end
          
          return links
          
        end
        
        def trigger; end
        
        # applicable 
        def self.applicable?( short_link )
          short_link =~ /^\/(index\.php){0,1}\?typ=[A-Z]{1}&xml$/
        end
        
      end
    
  end
end
