module TangoApp
  module Operator
    
    # Operator for Tutor resource
    #
    # @author Mckomo
    class Tutor < ::Tango::ETL::OperatorInterface
    
      # Extract tutor data from an XML element
      #
      # @param tutor_xml [Array|Nokogiri::XML::Element]
      # @return [Hash]
      def self.extract( tutor_xml )
        
        # Act depending on a class
        case tutor_xml
          when Nokogiri::XML::NodeSet
            
            tutors = []
                        
            tutor_xml.each do |t|
              
              tutor = Model::Tutor.new( { 
                name:       t.text.strip,
                moodle_id:  t.attr( "moodle" ).to_i
              } )
              
              tutors << tutor unless name_invalid?( tutor.name )
              
            end
            
            return tutors
            
          when Nokogiri::XML::Element
            
            return Model::Tutor.new( {
              name:       standardize_tutor_name( tutor_xml.attr( "nazwa" ) ),
              moodle_id:  tutor_xml.attr( "idcel" ).to_i
            })
            
        end
        
      end

      # Transform tutor params to match app logic
      #
      # @param tutor [Hash]
      # @return [Hash]
      def self.transform( tutor )
        tutor.tap do |t|
          t.id = Model::Tutor::next_id      
          # Update moodle id ( only negative numbers are accepted ) 
          t.moodle_id = tutor.moodle_id.abs if tutor.moodle_id.is_a? Integer
        end
      end
    
      # Load tutor into a storage
      #
      # @param tutors [Array]
      # @return [Object]
      def self.load( tutors )
        Model::Tutor.import( Model::Tutor.properties, tutors.map { |p| p.values }, validate: false )
      end
      
      private 
      
      def self.standardize_tutor_name( tutor_name )
        
        # Remove spaces from dash separator
        tutor_name = tutor_name.strip.gsub( ' - ', '' )
        
        name_part = [] 
        title_part = []
        
        # Split tutor name string to name part and title part
        tutor_name.split.each do |w|
          if /[[:upper:]]/u =~ w[0, 1] and /[[:lower:]]/u =~ w[1, 1]
            name_part << w.gsub( ',', '' )
          else
            title_part << w
          end
        end
        
        "#{title_part.join( ' ' )} #{name_part.reverse.join( ' ' )}".strip
        
      end
      
      def self.name_invalid?( name )
        name.empty? or /lektorat/iu =~ name
      end

    end
  end
end