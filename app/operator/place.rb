module TangoApp
  module Operator
    
    # Operator for Place resource
    #
    # @author Mckomo
    class Place < ::Tango::ETL::OperatorInterface
    
      # Extract place data from an XML element
      #
      # @param place_xml [Nokogiri::XML::Element]
      # @return [Hash]
      def self.extract( place_xml )
        
        @@parser ||= Helper::LocationParser.new

        # Act depending on XML element
        case place_xml.name
        when "plan-zajec"
          location_candidates = place_xml.attr( "nazwa" )
        when "sala" 
          location_candidates = place_xml.text
        end
        
        @@parser.parse( location_candidates ).map { |l|
          Model::Place.new( { 
            original_name: l.to_s( :original ),
            short_location: l.to_s( :short ), 
            full_location: l.to_s( :full ) 
          } )
        }

      end

      # Transform place params to match app logic
      #
      # @param place [Hash]
      # @return [Hash]
      def self.transform( place )
          place.tap do |p| 
            p.id = Model::Place::next_id
          end
      end
    
      # Load place into a storage
      #
      # @param places [Array]
      # @return [Object]
      def self.load( places )
        Model::Place.import( Model::Place.properties, places.map { |p| p.values }, validate: false )
      end

      private 
      
      # Standardize name location
      def self.standarize_name( location )
        tokens = location.split
      end
    
    end
  end
end