module TangoApp
  module Operator
    
    # Operator for Group resource
    #
    # @author Mckomo
    class Group < ::Tango::ETL::OperatorInterface
    
      # Extract group data from an XML element
      #
      # @param group_xml [Object] Could be Nokogiri::XML::Element or String
      # @return [Model::Group|Array]
      def self.extract( group_xml )
        
        case group_xml.name
        when "plan-zajec"
          Model::Group.new( name: group_xml.attr( 'nazwa' ) ) # create single group
        when "grupa"
          group_xml.text.split( "," ).map { |n| Model::Group.new( name: n.strip ) } # create list of groups
        end
        
      end

      # Transform group params to match app logic
      #
      # @param group [Group]
      # @return [Hash]
      def self.transform( group )
        group.tap do |g| 
          g.id = Model::Group::next_id
        end
      end
      
      # Load group into a storage
      #
      # @param groups [Array]
      # @return [Object]
      def self.load( groups )
        Model::Group.import( Model::Group.properties, groups.map { |g| g.values }, validate: false ) 
      end
      
      # Compose name of a elective group
      # 
      # @param group [Model::Group]
      # @param activity [Model::Activity]
      # @return String
      def self.elective_name( group, activity )
        "#{group.name} #{activity.name}"
      end
      
    end
  end
end