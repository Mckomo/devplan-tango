module TangoApp
  module Operator
    
    # Operator for Group resource
    #
    # @author Mckomo
    class Assignment < ::Tango::ETL::OperatorInterface
      
      # Transform participation
      #
      # @param participation [Hash]
      # @return [Hash]
      def self.transform( assigment )
          assigment.tap do |a| 
            a.id = Model::Assignment::next_id
          end
      end
      
      # Load group into a storage
      #
      # @param groups [Array]
      # @return [Object]
      def self.load( assigments )
        Model::Assignment.import( Model::Assignment.properties, assigments.map { |a| a.values }, validate: false ) 
      end
      
    end
    
  end
end