module TangoApp
  module Operator
    
    # Operator for Group resource
    #
    # @author Mckomo
    class Participation < ::Tango::ETL::OperatorInterface
      
      # Transform participation
      #
      # @param participation [Hash]
      # @return [Hash]
      def self.transform( participation )
          participation.tap do |p| 
            p.id = Model::Participation::next_id
          end
      end
      
      # Load group into a storage
      #
      # @param groups [Array]
      # @return [Object]
      def self.load( participations )
        Model::Participation.import( Model::Participation.properties, participations.map { |p| p.values }, validate: false ) 
      end
      
    end
    
  end
end