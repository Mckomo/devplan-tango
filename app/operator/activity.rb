module TangoApp
  module Operator
    
    # Operator for Activity resource
    #
    # @author Mckomo
    class Activity < ::Tango::ETL::OperatorInterface

      # Extract activity data from a XML element
      #
      # @param activity_xml [Nokogiri::XML::Element]
      # @return [Hash]
      def self.extract( activity_xml )
        
        return nil unless activity_xml
        
        # Get hash of activity params
        activity = Model::Activity.new
        
        # Set activity date
        activity.date =  activity_xml.at_xpath( 'termin' ).text
  
        # Set day of week
        activity.day_of_week = activity_xml.at_xpath( 'dzien' ).text
  
        # Set activity start time
        activity.starts_at = activity_xml.at_xpath( 'od-godz' ).text
  
        # Set time of the activity ending
        activity.ends_at = activity_xml.at_xpath( 'do-godz' ).text
  
        # Set name of the activity
        activity.name = activity_xml.at_xpath( 'przedmiot' ).text
  
        # Set category of the activity
        activity.category = ActiveSupport::Multibyte::Chars.new(activity_xml.at_xpath( 'typ' ).text).capitalize.to_s
        
        # Set notes of the activity
        activity.notes = activity_xml.at_xpath( 'uwagi' ) ? activity_xml.at_xpath( 'uwagi' ).text : ""

        # p activity.notes unless activity.notes.empty?
        
        activity
        
      end

      # Transform activity params to match app logic
      #
      # @param activity [Activity]
      # @return [Hash]
      def self.transform( activity, group_name = nil )
        
        # Set id
        activity.id = Model::Activity.next_id
        
        # By default set state of the activity to active ( 1 -> active )
        activity.state = 1
        
        # Category of the activity could say that it has been moved to other date
        # Change state to 3 ( 3 == postponed ) if so
        if activity.postponed?
          activity.state = 3
        # Change state to 2 ( 2 == has some remarks ) if the activity has notes
        elsif activity.notes.length > 0
          activity.state = 2 
        end
        
        begin
          if activity.notes
            activity.notes = activity.notes.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
          end
        rescue Encoding::UndefinedConversionError
          activity.notes = ''
        end 
        
        # Calculate a timestamp of the activity start time
        start_time = "#{activity.date} #{activity.starts_at}"
        activity.starts_at_timestamp = Time.parse( start_time ).to_i unless start_time.strip.empty?
        
        # Calculate a timestamp of the activity end time
        end_time = "#{activity.date} #{activity.ends_at}"
        activity.ends_at_timestamp = Time.parse( end_time ).to_i unless end_time.strip.empty?
  
        activity
  
      end
    
      # Load activities into a storage
      #
      # @param activities [Array]
      # @return [Object]
      def self.load( activities )
        Model::Activity.import( Model::Activity.properties, activities.map { |a| a.values }, validate: false )
      end
    
    end
  end
end