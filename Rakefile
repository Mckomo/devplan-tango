require 'rake/testtask' 

# By default go with run task
task :default => "test:unit"

desc "Setup environment"
task :env, [:env] do |t, args|
  
  # Set environment constant
  TANGO_ENV = args[:env] || 'development'

  # Set up gems
  require 'rubygems'
  require 'bundler/setup'
  require 'tango'
    
  # Set paths constants
  ROOT_PATH = File.dirname( __FILE__ )
  APP_PATH = File.join( ROOT_PATH, '/app' )
  
end

namespace "tango" do
  
  desc "Prepare environment for Tango"
  task :prepare, [:env] => :env do
    Dir.mkdir( "#{ROOT_PATH}/log" )
    Dir.mkdir( "#{ROOT_PATH}/tmp" )
  end
  
  desc "Load Tango app"
  task :setup, [:env] => :env do
    require_relative 'app/bootstrap'
  end
  
  desc "Run Tango"
  task :run, [:env] => ["db:setup", "tango:setup"] do
    
    config = YAML.load_file( File.join( ROOT_PATH, 'config', 'app.yml' ) )[TANGO_ENV] 
    
    if ['production', 'staging'].include?(TANGO_ENV)
      logger_output = "./log/#{Time.new.to_s}.log" # a file
    else
      logger_output = STDOUT # Terminal window
    end
    
    # Create a logger
    logger = Logger.new( logger_output )
    logger.level = Logger::INFO
    
    # Init Tango app
    tango = TangoApp::App.new( config: config, parser: Nokogiri::XML, logger: logger )
    
    # Load ETL operators
    Tango::Kernel.module_classes( TangoApp::Operator ).each do |o|
      tango.register_operator( o )
    end
    
    # Load models
    Tango::Kernel.module_classes( TangoApp::Model ).each do |m|
      tango.register_model( m )
    end

    # Load handlers
   Tango::Kernel.module_classes( TangoApp::Handler ).each do |h|
      tango.register_handler( h )
    end

    # Run Tango using one of the Octopus shards
    begin
      tango.run
    rescue Interrupt
      logger.fatal( "Tango was terminated by a interrupt signal." )
    ensure
      logger.close
    end
    
  end

end

namespace "db" do
  
  desc "Setup database connection"
  task :setup, [:env] => :env do
    
    require 'active_record'
    require 'mysql2'
    
    ActiveRecord::Base.configurations = YAML::load( File.open( "config/database.yml" ) )
    ActiveRecord::Base.establish_connection TANGO_ENV.to_sym
    
  end
  
  desc "Run database migrations"
  task :migrate, [:env] => :setup do
    ActiveRecord::Migrator.migrate( "db/migration" )
  end
  
  desc "Drop database migrations"
  task :drop, [:env] => :setup do
    ActiveRecord::Migrator.down( "db/migration" )
  end

end

namespace :test do

  task :unit do
    FileList['./test/unit/*/*_test.rb'].each { |f| require f }
  end

  task :integration do
    FileList['./test/integration/*/*_test.rb'].each { |f| puts %x{ruby #{f}} }
  end

end
