#Changelog

###0.1.0 - June 2013
- Tango prototype arrives.

###0.1.1 - 23/11/2013 
- Tango has now full functionality. Yet no tests written.

###0.1.2 - 24/11/2013 
- Support for elective activities.

###0.2.0 - 02/12/2013
- Reached much better performance
- Introduced operators as conductors of ETL process
- Introduced resource cache and data buffered insertion
- Implemented resource versioning

###0.2.1- 12/2013
- Introduced 'after', 'before' filters
- Full automation of cache system
- Database synchronization with Cash

###0.2.2 - 25/02/2014
- Changed database manager
- Code refactoring
- Removed encoding problems

### 0.3.0 - 15/04/2014
- devPlan Tango is now using [Tango gem](https://rubygems.org/gems/tango-etl)

### 0.3.1 - 29/07/2014
- Fallowed changes in Tango-ETL
- Change database structure. Now activity has many teachers and places
- Added uniform format of places' locations
- Added tests

### 0.3.2 - 01/11/2014
- Added UTF-8 support for capitalization of category name in activity
- Fixed elective activities
- Update of location parser