#!/usr/bin/env bash

HOME_DIR=~devplan
TANGO_DIR=$HOME_DIR/production/tango
RUBY_ENV=`cat $HOME_DIR/production/tango/.ruby-env`

# Load Ruby environment
source /.rvm/environments/$RUBY_ENV

# Change dir to Tango root
cd $HOME_DIR/production/tango

# Run Tango
rake tango:run[production]
