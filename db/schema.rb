ActiveRecord::Schema.define do
  
  create_table :activities do |t|
    t.string      :name
    t.string      :category # equivalent of 'type' in this case
    t.text        :notes
    t.integer     :state # 0 -> inactive, 1 -> active, 2 -> has some remarks, 3 -> postponed
    t.string      :date # with format yyyy-mm-dd
    t.string      :day_of_week
    t.string      :starts_at # with format hh:mm 
    t.string      :ends_at # with format hh:mm
    t.integer     :starts_at_timestamp
    t.integer     :ends_at_timestamp
  end
  
  create_table :groups do |t|
    t.string   :name
  end
  
  create_table :assignments do |t|
    t.integer   :activity_id
    t.integer   :tutor_id
  end
  
  create_table :participations do |t|
    t.integer   :activity_id
    t.integer   :group_id
  end
  
  create_table :localizations do |t|
    t.integer   :activity_id
    t.integer   :place_id
  end
  
  create_table :places do |t|
    t.string   :original_name 
    t.string   :short_location
    t.string   :full_location
  end
  
  create_table :tutors do |t|
    t.string  :name
    t.integer :moodle_id
  end
    
end