class CreateTutors < ActiveRecord::Migration
  
  def up
    create_table :tutors do |t|
      t.string   :name
      t.string   :moodle_id
    end
  end
  
  def down
    drop_table :tutors
  end
   
end