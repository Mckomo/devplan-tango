class CreateActivities < ActiveRecord::Migration
  
  def up
    
    create_table :activities do |t|
      t.string      :name
      t.string      :category # equivalent of 'type' in this case
      t.text        :notes
      t.integer     :state # 0 -> inactive, 1 -> active, 2 -> has some remarks, 3 -> postponed
      t.string      :date # with format yyyy-mm-dd
      t.string      :day_of_week
      t.string      :starts_at # with format hh:mm 
      t.string      :ends_at # with format hh:mm
      t.integer     :starts_at_timestamp
      t.integer     :ends_at_timestamp
    end
    
  end
  
  def down
    drop_table :activities
  end
  
end