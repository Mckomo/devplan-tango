class CreatePlaces < ActiveRecord::Migration
  
  def up
    create_table :places do |t|
      t.string 	 :original_name 
      t.string   :short_location
      t.string   :full_location
    end
  end
  
  def down
    drop_table :places
  end
   
end