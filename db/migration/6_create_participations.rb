class CreateParticipations < ActiveRecord::Migration
  
  def up
    create_table :participations do |t|
      t.integer   :activity_id
      t.integer   :group_id
    end
  end
  
  def down
    drop_table :participations
  end
   
end