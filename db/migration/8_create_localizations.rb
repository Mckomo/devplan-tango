class CreateLocalizations < ActiveRecord::Migration
  
  def up
    create_table :localizations do |t|
      t.integer   :activity_id
      t.integer   :place_id
    end
  end
  
  def down
    drop_table :localizations
  end
   
end