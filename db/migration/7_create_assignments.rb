class CreateAssignments < ActiveRecord::Migration
  
  def up
    create_table :assignments do |t|
      t.integer   :activity_id
      t.integer   :tutor_id
    end
  end
  
  def down
    drop_table :assignments
  end
   
end