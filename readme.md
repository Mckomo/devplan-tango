# devPlan Tango

Tango is 'big' steamy ETL. Its job is to scrap content from [Cracow University of Economics timetable website](http://planzajec.uek.krakow.pl/), extract necessary data and save it to data base. Without Tango [Cash](https://bitbucket.org/Mckomo/devplan-cash) would be nothing - keep that on mind.

For father reference go to [documentation](https://bitbucket.org/Mckomo/devplan-docs) of devPlan project.